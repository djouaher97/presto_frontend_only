fetch('./2021-10-06-js-presto-02.json').then(data => data.json())
.then(anns => {
    
    let searchAnn = document.querySelector('#search-ann')
    let searchLoc = document.querySelector('#search-loc')
    let cardAnnWrapper = document.querySelector('#card-ann-wrapper')
    let selectCategory = document.querySelector('#select-category')
    let minPriceRange = document.querySelector('#minPriceRange')
  
    let maxPriceRange = document.querySelector('#maxPriceRange')
    let minPrice = document.querySelector('#minPrice')
    let maxPrice = document.querySelector('#maxPrice')
    let glideWrap = document.querySelector('#glide-wrapper')
    console.log(glideWrap)



    
    function populateAnn(anns) {
        cardAnnWrapper.innerHTML =''
        anns.forEach((ann) => {
            let card = document.createElement('div')
            card.classList.add('col-12', 'col-lg-6', 'col-xl-4','pb-3')
            card.innerHTML = 
            `<div class="bg-image-card d-flex flex-column justify-content-end px-3 position-relative" >
            <div class="text-ann">

            <h4>
            <a class="text-decoration-none fw-bolder text-white"href="#">${ann.name}</a>
            <p class='text-warning mt-3 fw-bold fs-5'>€${ann.price}</p>
          </h4>
          <p class='text-white'>Categoria: <strong class="text-warning">${ann.category}</strong></p>

          <div class="d-flex justify-content-between">
            <p class='text-white'>
            <i class="fa-solid fa-location-dot text-warning pin-location"></i>
            <strong>${ann.location}</strong></p>

            <button type="button" class="" data-bs-toggle="modal" data-bs-target="#exampleModal">
                <a class="text-decoration-none"href="#" >
                    <i class="text-white eyes fas fa-eye  d-flex justify-content-center align-items-center"></i>
                </a>
          </button>

            
                 
          </div>
           </div>
            
            
            
            </div>
        
            </div>`
            

            cardAnnWrapper.appendChild(card)

        })
    }


    

    // FUNZIONI DI RICERCA -----

    function searchWord(str) {

        let filtered = anns.filter(ann => ann.name.toLowerCase().includes(str.toLowerCase()))

        if (filtered.length > 0) {
            populateAnn(filtered)
        } else {
            cardAnnWrapper.innerHTML = ''
            let card = document.createElement('div')
            card.innerHTML = `<h2>Nessun Annuncio Trovato MANNAGGETTA</h2>`
            cardAnnWrapper.appendChild(card)
            
        }
    }

    searchAnn.addEventListener('input', () => {
        searchWord(searchAnn.value)
        console.log(searchAnn)
    })


    function searchPosition(str) {

        let filtered = anns.filter(ann => ann.location.toLowerCase().includes(str.toLowerCase()))
        if (filtered.length > 0) {
            populateAnn(filtered)
        } else {
            cardAnnWrapper.innerHTML = ''
            let card = document.createElement('div')
            card.innerHTML = `<h2>Nessun Annuncio Trovato In Questa Regione</h2>`
            cardAnnWrapper.appendChild(card)
            
        }
    }

    searchLoc.addEventListener('input', () => {
        searchPosition(searchLoc.value)
    })


    function populateCategory () {
        let annCategory = anns.map(ann => ann.category)
        let uniqueCategory = []

        annCategory.forEach(category => {
            if(!uniqueCategory.includes(category)){
                uniqueCategory.push(category);
            }
        })
        
        uniqueCategory.forEach(category => {
            let option = document.createElement('option')
            option.value = category
            option.innerHTML = category

            

            selectCategory.appendChild(option)
        })
    }

    function filterByCategory(category) {
        let filtered = anns.filter(ann => ann.category === category)
        populateAnn(filtered)
    }

    populateCategory()
    filterByCategory()

    // SLIDERE PRICE ---------------------

    selectCategory.addEventListener('input', () => {
        filterByCategory(selectCategory.value);
        if (selectCategory.value == 'all') {
            populateAnn(anns)
        }
    })


    function rangeSlider () {
        let minMax = anns.map(ann => Number(ann.price))
        let min = Math.floor(Math.min(...minMax))
        let max = Math.ceil(Math.max(...minMax))

        
        // Imposto gli atriibuti del input al max e al min degli annunci che ho caricato
        
        maxPriceRange.min = min;
        maxPriceRange.max = max;
        maxPriceRange.value = max;

        minPriceRange.min = min;
        minPriceRange.max = max;
        minPriceRange.value = min;
        
        // imposto le etichette        
        minPrice.innerHTML = `€${min}`
        maxPrice.innerHTML = `€${max}`
    }

    function changeMinPrice(){
        let selectedMin = Number(minPriceRange.value)
        let selectedMax = Number(maxPriceRange.value)

        
        if (selectedMin >= selectedMax) {
            minPriceRange.value  = selectedMax - 200
        } else{
            minPrice.innerHTML = selectedMin + '€'
        }

        filterByPrice()

    }

    function changeMaxPrice(){
        let selectedMin = Number(minPriceRange.value)
        let selectedMax = Number(maxPriceRange.value)

        
        if (selectedMax <= selectedMin) {
            maxPriceRange.value  = selectedMin + 200
        } else{
            maxPrice.innerHTML = selectedMax + '€'
        }
        

        filterByPrice()

    }


    function filterByPrice() {

        let selectedMin = Number(minPriceRange.value)
        let selectedMax = Number(maxPriceRange.value)

        let filtered = anns.filter( ann => (ann.price >= selectedMin) && (ann.price <= selectedMax))
        
        console.log(filtered);

        console.log(selectedMin);
        console.log(selectedMax);
        populateAnn(filtered)

    }




    minPriceRange.addEventListener( 'input' , () =>{
        changeMinPrice();
    })

    maxPriceRange.addEventListener('input' , () =>{
        changeMaxPrice();
    })






    rangeSlider()






    populateAnn(anns) /*funzione che popula gli annunci */

    
   
})   /*chiusura del then della fetch */






let colNav = document.querySelector('#col-nav')
let prestoNav = document.querySelector('#presto-nav')

window.addEventListener('scroll', () => {
    if(window.pageYOffset > 600) {
        colNav.classList.add('px-0', 'fixed-top') 
        colNav.classList.remove('col-md-10')
        prestoNav.classList.add('border-radius', 'blur-nav')
    }else {
        colNav.classList.remove('px-0', 'fixed-top', 'border-radius')
        colNav.classList.add('col-md-10')
        prestoNav.classList.remove('border-radius', 'blur-nav')
    }
})



if (document.querySelector('.glide') != null) {
    new Glide('.glide', {
        type: 'carousel',
        startAt: 0,
        perView: 4,
        // autoplay: 3000,
        gap:50,
        breakpoints : {
            768: {perView: 1},
            1000: {perView: 2},
            1204: {perView: 3},
        },
    }).mount()
}
    


  

